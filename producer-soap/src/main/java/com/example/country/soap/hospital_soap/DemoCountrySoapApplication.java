package com.example.country.soap.hospital_soap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

/*
@SpringBootApplication
public class DemoCountrySoapApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoCountrySoapApplication.class, args);
    }

}

 */

//////============= RUN AS TOMCAT - WAR ============
@SpringBootApplication
public class DemoCountrySoapApplication extends SpringBootServletInitializer {
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(DemoCountrySoapApplication.class);
    }

    public static void main(String[] args) {
        SpringApplication.run(DemoCountrySoapApplication.class, args);
    }
}
