package com.example.rpcclient.rpcclient;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

/*
@SpringBootApplication
public class RpcClientApplication {
    public static void main(String[] args) {

        SpringApplication.run(RpcClientApplication.class, args);
    }

}
*/

//////============= RUN AS TOMCAT - WAR ============
@SpringBootApplication
public class RpcClientApplication extends SpringBootServletInitializer {
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(RpcClientApplication.class);
    }

    public static void main(String[] args) {
        SpringApplication.run(RpcClientApplication.class, args);
    }
}
